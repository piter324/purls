# PURLS - Peter's URL Shortener
Minimalistic and fast URL shortener. Written in Go, with Gin and GORM 2.0. SQLite as a database.

Originally written for "Client Engager" system, thus `s.engager.app` is used as a server name or root URL for `consts` and `deployment/purls.conf`. It should of course be changed when deploying for other domains.
## CHECKLIST - releasing new version
- [ ] Bump version number in `consts`
- [ ] Check if all configuration in `deployment` directory is up-to-date
- [ ] Compile the server application with `go build main.go`
- [ ] Tag new version in repository
## Deployment
This application should be run as a daemon using standard operating system mechanisms. It does NOT require root privileges, so it is better to run it as a standard user. In itself, it does NOT feature any kind of authentication nor rate limiting, so they should be configured in reverse proxy and/or using firewalls.

Only `/:shortlink` endpoint should be exposed to the Internet, and only if decent rate limiting is applied to minimize chances of a successful database sweep with brute forcing each combination of characters in a shortlink. Minimal length of a shortlink is 5 characters but is increased for new links when the number of existing ones is greater than 75% of the current combination count: `(10 digits + 26 uppercase letters + 26 lowercase letters)^5 = 62^5 = 916,132,832`.

`/api` endpoints should NOT be exposed to the Internet as they allow for viewing all links in the system and for creating new ones.
### Deploying on Debian with Nginx as regular user's system service
1. Replace `SHORTLINK_PREFIX` value in `consts/setup.go` with your domain name. **Remember** to start with the protocol `http://` or `https://` and finish with a slash `/`. Alter other const values if necessary.
1. Create deploy package using `prepareDeploymentPackage.sh` script
1. Put the package on the server somewhere in your user's home directory, eg. `~/purls`
1. Untar it with: `tar -zxvf PACKAGE_FILENAME`
1. Replace `s.engager.app` in server block config `deployment/purls.conf` with your domain name. **FQDN only** - no protocol prefix or slashes.
1. Copy server block config to `/etc/nginx/sites-available` and create a symlink in `sites-enabled`. Server block config comes preconfigured with a rate limiting zone allowing 1 request per second from each public IP.
1. Replace `PURLS_EXEC_DIR` in service definition with **absolute** path of the actual directory PURLS is deployed to.
1. Copy service definition to `~/.config/systemd/user`
1. Allow for starting services for your username without the user having to log in: `sudo loginctl enable-linger YOUR_USERNAME`
1. Start the service: `systemctl --user start purls`. **NO SUDO** for commands with `--user` flag because we want to run this in regular user scope.
1. Check if the service is running: `systemctl --user status purls`.
1. Enable the service to start on boot: `systemctl --user enable purls`.
1. Protect custom port PURLS is running on (`8005` by default) with a firewall to allow access only from permitted IPs. Server block already does not let requests to `/api` endpoints through reverse proxy. Alternatively, reconfigure server block to apply some authentication to `/api` endpoints instead of straight up returning `403 Forbidden`. PURLS does not implement any authentication on its own.

### Database
- SQLite database file will be created in the deployment directory on first app launch with auto-migration taking care of creating tables.
- Feel free to add support for other databases. A function responsible for connecting to a DB is in `models/setup.go`.

### Errors
If `Failed to connect to bus` error occurs when issuing systemctl commands, export below variables and add them to `.bashrc` for future use:
```bash
export XDG_RUNTIME_DIR="/run/user/$UID"
export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"
```
## Consts
Remember to recompile after changing consts below.
```go
package consts

const VERSION string = "1.0.0"

// Below are deployment-specific
const LISTEN_ADDR_PORT string = "0.0.0.0:8005"
const SHORTLINK_PREFIX string = "https://s.engager.app/" // replace with your domain
const INITIAL_MIN_LINK_LENGTH int = 5

```
## Endpoints
All endpoints can return generic errors not included in the list below. HTTP code is always `>= 400` and response body is either JSON for API endpoints or plaintext for `/:shortlink` endpoint.
```json
{
    "error":"Some error description"
}
```
### GET /
#### Response:
```json
{
    "data": "Hallo Welt!"
}
```

### GET /:shortlink
#### Request
Address: `https://s.engager.app/iUPmR`

#### Response
**Success**

Redirected to the longlink with code `303 See Other`

**Error**

Error code 404 Not found with plaintext message:
```
Link not found
```
### GET /api/v1/links
Should be limited by firewalls and reverse proxy only to internal network.
#### Response
```json
{
    "count": 2,
    "data": [
        {
            "shortlink": "https://s.engager.app/iUPmR",
            "longlink": "https://dev.engager.app/customer/longlink",
            "createdAt": "2021-11-02T14:39:14.79599412Z",
            "lastUsedAt": {
                "Time": "2021-11-02T14:42:13.591593943Z",
                "Valid": true
            },
            "createdBy": "dev"
        },
        {
            "shortlink": "https://s.engager.app/aYsm2",
            "longlink": "https://dev.engager.app/customer/anotherLonglink",
            "createdAt": "2021-11-02T17:32:28.950393691Z",
            "lastUsedAt": {
                "Time": "0001-01-01T00:00:00Z",
                "Valid": false
            },
            "createdBy": "dev"
        }
    ]
}
```
### GET /api/v1/links/:linkType
#### Request
**URL params**
```ebnf
linkType := shortlink | longlink
```
**Query params**
```ebnf
linkValue := shortlink value or longlink value
```
#### Response
**Success**
```json
{
    "data": {
        "shortlink": "https://s.engager.app/iUPmR",
        "longlink": "https://dev.engager.app/customer/longlink",
        "createdAt": "2021-11-02T14:39:14.79599412Z",
        "lastUsedAt": {
            "Time": "2021-11-02T14:42:13.591593943Z",
            "Valid": true
        },
        "createdBy": "dev"
    }
}
```
**Error**
```json
{
    "error": "Object not found"
}
```
### POST /api/v1/links
#### Request
**Body**
```json
{
    "longlink":"https://dev.engager.app/customer/differentLonglink",
    "createdBy":"dev"
}
```
#### Response
**Success**
```json
{
    "data": {
        "shortlink": "https://s.engager.app/aYsm2",
        "longlink": "https://dev.engager.app/customer/differentLonglink",
        "createdAt": "2021-11-02T17:32:28.950393691Z",
        "lastUsedAt": {
            "Time": "0001-01-01T00:00:00Z",
            "Valid": false
        },
        "createdBy": "dev"
    }
}
```
## Notes
### lastUsedAt
This variable is a timestamp indicating when the link object was last used to redirect someone to the underlying longlink. It gets updated when `/:shortlink` endpoint is called. Due to technical difficulties with handling NULL values whenever `lastUsedAt.Valid` is `false`, the whole variable `lastUsedAt` should be considered NULL.