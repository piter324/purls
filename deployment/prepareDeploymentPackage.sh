#!/bin/bash

PACKAGE_NAME=purls-deploy-package.tgz

cd ../ &&
go build main.go && 
cd deployment/ &&
cp ../main . &&
rm -f $PACKAGE_NAME &&
tar -zcvf $PACKAGE_NAME * &&
echo "Done!"