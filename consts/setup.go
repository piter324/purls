package consts

const VERSION string = "1.0.0"

// Below are deployment-specific
const LISTEN_ADDR_PORT string = "0.0.0.0:8005"
const SHORTLINK_PREFIX string = "https://s.engager.app/"
const INITIAL_MIN_LINK_LENGTH int = 5
