package main

import (
	"log"
	"net/http"
	"purls/consts"
	"purls/controllers"
	"purls/models"

	"github.com/gin-gonic/gin"
)

func main() {
	log.Default().Println("Starting server version:", consts.VERSION)

	router := gin.Default()
	models.ConnectDatabase()

	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "Hallo Welt!"})
	})

	router.GET("/:shortlink", controllers.RedirectFromShortlink)

	api := router.Group("/api")
	{
		v1 := api.Group("/v1")
		{
			v1.GET("/links", controllers.GetAllLinks)
			v1.GET("/links/:linkType", controllers.GetLink) // linkValue in query params
			v1.POST("/links", controllers.CreateLink)
		}
	}

	router.Run(consts.LISTEN_ADDR_PORT)

	// Experiments with binding to UNIX socket
	// listener, err := net.Listen("unix", "/tmp/gino.sock")
	// if err != nil {
	// 	panic("Cannot bind to socket")
	// }
	// router.RunListener(listener)
}
