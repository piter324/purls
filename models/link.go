package models

import (
	"database/sql"
	"time"
)

type Link struct {
	Shortlink  string       `json:"shortlink" gorm:"primary_key"`
	Longlink   string       `json:"longlink" gorm:"index"`
	CreatedAt  time.Time    `json:"createdAt" gorm:"<-:create;autoCreateTime"`
	LastUsedAt sql.NullTime `json:"lastUsedAt" gorm:"default:null"`
	CreatedBy  string       `json:"createdBy"`
}

type CreateLinkInput struct {
	Longlink  string `json:"longlink" binding:"required"`
	CreatedBy string `json:"createdBy" binding:"required"`
}
