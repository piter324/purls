package controllers

import (
	"database/sql"
	"log"
	"math"
	"net/http"
	"purls/consts"
	"purls/models"
	"time"

	"github.com/dchest/uniuri"
	"github.com/gin-gonic/gin"
)

const SHORTLINK_PREFIX string = consts.SHORTLINK_PREFIX
const INITIAL_MIN_LINK_LENGTH int = consts.INITIAL_MIN_LINK_LENGTH

func _GetLinkByLongOrShortlink(linkType string, linkValue string, linkRef *models.Link) error {
	if err := models.DB.Where(linkType+" = ?", linkValue).First(&linkRef).Error; err != nil {
		return err
	}
	return nil
}

// GET ROOT_URL/:shortlink
func RedirectFromShortlink(c *gin.Context) {
	var link models.Link
	if err := _GetLinkByLongOrShortlink("shortlink", SHORTLINK_PREFIX+c.Param("shortlink"), &link); err != nil {
		c.String(http.StatusNotFound, "Link not found")
		return
	}
	link.LastUsedAt = sql.NullTime{Time: time.Now(), Valid: true}
	models.DB.Save(&link)

	c.Redirect(303, link.Longlink) // 303 See Other
}

// ----- API ROUTE HANDLERS -----

// GET /api/v1/links
func GetAllLinks(c *gin.Context) {
	var links []models.Link
	models.DB.Find(&links)

	c.JSON(http.StatusOK, gin.H{"data": links, "count": len(links)})
}

// GET /api/v1/links/:linkType/:linkValue
func GetLink(c *gin.Context) {
	linkType := c.Param("linkType")
	linkValue := c.Query("linkValue")

	if linkType != "shortlink" && linkType != "longlink" {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": "Unsupported link type"})
		return
	}

	var link models.Link
	if err := _GetLinkByLongOrShortlink(linkType, linkValue, &link); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Object not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": link})
}

// POST /api/v1/links
func CreateLink(c *gin.Context) {
	// Validate input
	var input models.CreateLinkInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusUnprocessableEntity, gin.H{"error": err.Error()})
		return
	}

	// Check if longlink does not have a shortlink already - if it does, return it
	var existingLink models.Link
	if err := _GetLinkByLongOrShortlink("longlink", input.Longlink, &existingLink); err == nil {
		log.Default().Println("Existing link found, returning")
		c.JSON(http.StatusOK, gin.H{"data": existingLink})
		return
	}

	// Get current link count
	var allLinksCount int
	models.DB.Model(&models.Link{}).Count(&allLinksCount)

	// Compute new link length to minimize the rish of clashing with existing links
	minLinkLength := INITIAL_MIN_LINK_LENGTH
	charsCount := float64(len(uniuri.StdChars))
	for ; float64(allLinksCount) > 0.75*math.Pow(charsCount, float64(minLinkLength)); minLinkLength++ {
		log.Default().Println("Increasing link length from:", minLinkLength)
	}
	log.Default().Println("Link length:", minLinkLength)

	var finalShortlink string
	// Generate random link of a given length
	for {
		shortlink := uniuri.NewLen(minLinkLength)

		// Check if a link does not exist yet
		var existingShortlinkCount int
		models.DB.Model(&models.Link{}).Where("shortlink = ?", SHORTLINK_PREFIX+shortlink).Count(&existingShortlinkCount)

		if existingShortlinkCount < 1 {
			finalShortlink = SHORTLINK_PREFIX + shortlink
			break
		}
		log.Default().Println("Shortlink already taken", shortlink, ", generating new one")
	}

	// Create a new link object
	link := models.Link{
		Shortlink: finalShortlink,
		Longlink:  input.Longlink,
		CreatedBy: input.CreatedBy,
	}
	if err := models.DB.Create(&link).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": link})
}
